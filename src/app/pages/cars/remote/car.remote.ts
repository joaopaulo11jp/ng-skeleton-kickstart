import { BaseRemote, Remote } from '../../../core/remote';
import { Car } from '../shared/car';
import { Observable } from 'rxjs';
import { CarInterface } from '../shared/car.interface';
import { Injectable } from '@angular/core';

@Remote({
  baseUrl: 'api/cars'
})
@Injectable()
export class CarRemote extends BaseRemote<Car>{

  getCarNameList(): Observable<CarInterface[]>{
    return this.get<CarInterface[]>("");
  }
}
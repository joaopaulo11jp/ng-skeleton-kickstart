import { RemoteModel } from '../../../core/remote/remote.model.interface';

export class Car implements RemoteModel{
  id: number;
  name: string;
}
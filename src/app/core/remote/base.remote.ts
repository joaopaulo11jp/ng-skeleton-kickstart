import {RemoteModel} from './remote.model.interface';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Injectable, Inject } from '@angular/core';
import { RemoteSerializer } from './serializer/serializer';
import { BaseRemoteModule } from './base.remote.module';

interface RemoteParams<T> {
  options?: Object;
  serializer?: RemoteSerializer
}

@Injectable()
export class BaseRemote<T extends RemoteModel>{

  protected baseUrl: string

  constructor(@Inject(HttpClient) protected http: HttpClient){}

  protected get<Z>(url: string, params?: RemoteParams<Z>): Observable<Z> {
    return this.http.get<Z>(this.baseUrl, params ? params.options : {})
            .pipe(
              map(data => this.extractMap(data, params))
            );
  }

  protected post<Z>(url: string, body: Z, params?: RemoteParams<Z>): Observable<Z> {
    let treatedBody: Z = params && params.serializer && params.serializer.toRemote?
                          params.serializer.toRemote(body) : body;
    return this.http.post<Z>(this.baseUrl + url, treatedBody, params ? params.options : {})
            .pipe(
              map(data => this.extractMap(data, params))
            );
  }

  protected put<Z>(url: string, body: Z, params?: RemoteParams<Z>): Observable<Z> {
    let treatedBody: Z = params && params.serializer && params.serializer.toRemote?
                          params.serializer.toRemote(body) : body;
    return this.http.put<Z>(this.baseUrl + url, treatedBody, params ? params.options : {})
            .pipe(
              map(data => this.extractMap(data, params))
            );
  }

  protected delete(url: string, params?: RemoteParams<T>): any {
    return this.http.delete(this.baseUrl + url);
  }

  public getOne(id: number, serializer?: RemoteSerializer): Observable<T> {
    return this.get<T>(`${this.baseUrl}/${id}`, {serializer});
  }

  public getList(serializer?: RemoteSerializer): Observable<T[]> {
    return this.get<T[]>(this.baseUrl, {serializer});
  }

  public save(object: T, serializer?: RemoteSerializer): Observable<T> {
    return this.post<T>(this.baseUrl, object, {serializer});
  }

  public update(object: T, serializer?: RemoteSerializer): Observable<T> {
    return this.put<T>(this.baseUrl, object, {serializer});
  }

  public remove(id: number): any {
    return this.delete(`${this.baseUrl}/${id}`);
  }

  private extractMap(data, params){
      return params &&
              params.serializer &&
              params.serializer.fromRemote ?
                Array.isArray(data) ? data.map((data) => {params.serializer.fromRemote(data)}) 
                : 
                params.serializer.fromRemote(data) 
              : 
              data;
  }

  private getUrl(): string {
    return this.baseUrl;
  }
}
import { CarService } from './service/car.service';
import { CarRemote } from './remote/car.remote';
import { bindRemoteToService } from 'src/app/core/utils';

export let carsProvider = [
  {
    provide: CarRemote,
    useClass: CarRemote
  },
  {
    provide: CarService,
    useFactory: (carRemote: CarRemote) => bindRemoteToService(carRemote, new CarService()),
    deps: [CarRemote]
  }
]
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { BaseRemote } from '../remote/base.remote';

@Injectable()
export class BaseService<T extends BaseRemote<any>> {

  private remote: T;

  public setRemote(remote: T): void{
    this.remote = remote;
  }

  protected getRemote(): T{
    return this.remote;
  }

  protected serializeFromRemote(observable: Observable<any>, serializerFunction: Function): Observable<any>{
    return observable.pipe(
              map(data => !Array.isArray(data) ? 
              serializerFunction(data)
              :
              data.map((data) => serializerFunction(data)))
    )
  }
}
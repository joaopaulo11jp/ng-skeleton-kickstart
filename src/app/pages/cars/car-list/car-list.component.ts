import { Component, OnInit } from '@angular/core';

import { Car } from '../shared/car';
import { CarInterface } from '../shared/car.interface';
import { CarService } from '../service/car.service';

@Component({
  selector: 'app-car-list',
  templateUrl: './car-list.component.html',
  styleUrls: ['./car-list.component.sass']
})
export class CarListComponent implements OnInit {
  title = 'ng-skeleton';
  cars: Car[];
  carNames: CarInterface[];

  constructor(private carService:CarService){}

  ngOnInit(){
    this.getCars();
    this.listCarNames();
  }
  
  getCars(): void{
    this.carService.listCars()
      .subscribe(cars => {
        console.log(cars);
        this.cars = cars
      });
  }

  listCarNames(): void{
    this.carService.listCarNames()
      .subscribe(carNames => {
        console.log(carNames)
        this.carNames = carNames;
      });
  }

}

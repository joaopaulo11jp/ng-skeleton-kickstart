interface RemoteDecorator{
  baseUrl: string
}

export function Remote(params: RemoteDecorator){
  return function Remote<T extends {new(...args:any[]):{}}>(constructor:T){
    return class extends constructor {
      baseUrl = params.baseUrl; 
    }
  }
}
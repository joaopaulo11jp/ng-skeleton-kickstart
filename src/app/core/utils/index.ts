import { BaseService } from '../service/base.service';
import { BaseRemote } from '../remote/base.remote';

export let bindRemoteToService = (remote: BaseRemote<any>, service: BaseService<BaseRemote<any>>) => {
  service.setRemote(remote);
  return service;
}
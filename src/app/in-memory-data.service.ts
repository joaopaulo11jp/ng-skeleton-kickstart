import { Injectable } from '@angular/core';
import { InMemoryDbService, ResponseOptions } from 'angular-in-memory-web-api';

@Injectable({
  providedIn: 'root'
})
export class InMemoryDataService implements InMemoryDbService{

  createDb(){
    const cars = [
      {id:1, name: 'Palio'},
      {id:2, name: 'Celta'},
      {id:3, name: 'Chevete'},
      {id:4, name: 'Kadett'},
      {id:5, name: 'Gol'},
      {id:6, name: 'Opala'},
      {id:7, name: 'Monza'},
      {id:8, name: 'Escort'},
      {id:9, name: 'Marajó'},
      {id:10, name: 'Belina'},
      {id:11, name: 'Del rey'},
      {id:12, name: 'Fusca'}
    ]
    return {cars};
  }

  protected responseInterceptor(res: ResponseOptions, ri: RequestInfo): ResponseOptions{
    res.body = res.body['data'];
    return res;
  }
}

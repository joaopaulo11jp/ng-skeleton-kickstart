export interface RemoteSerializer {
  fromRemote?(obj:any): any
  toRemote?(obj: any): any
}

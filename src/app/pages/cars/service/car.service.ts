import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Car } from '../shared/car';
import { CarRemote } from '../remote/car.remote';
import { CarInterface } from '../shared/car.interface';
import { BaseService } from '../../../core/service';

@Injectable()
export class CarService extends BaseService<CarRemote>{

  listCars(): Observable<Car[]>{
    return this.serializeFromRemote(this.getRemote().getList(), (car => {
      console.log(car);
      car.name = car.name.toUpperCase();
      return car;
    }));
  }

  listCarNames(): Observable<CarInterface[]>{
    return this.getRemote().getCarNameList();
  }
}
